#!/usr/bin/env python
# coding=utf-8
#
# build.py - based on punk-otf build script
#
# Copyright 2010-2022 Khaled Hosny <khaled@aliftype.com>
# Copyright 2022-2023 Rajeesh KV <rajeeshknambiar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys
import fontforge
import glob
import subprocess
import tempfile
import configparser

#import pdb

MLM2_SCRIPT_TAG = "mlm2"

def is_script_latin(ucode):
    #Is this decimal unicode value ASCII?
    return ucode >= 0 and ucode <= 255

def is_script_malayalam(ucode):
    #Is this decimal unicode value a Malayalam character?
    return ucode >= 3328 and ucode <= 3455

def ot_script_from_ucode(ucode):
    #OpenType script tag for the character (decimal)
    if is_script_latin(ucode):
        return 'latn'
    if is_script_malayalam(ucode) or ucode == -1:
        return 'mlm2'
    return 'DFLT'

class FontBuilderSVG():

    def __init__(self, configfile, outfmt):

        self.alternates = {'DFLT':{}, 'latn':{}, 'mlm2':{}}
        self.glyhpname_ucode = {}
        self.outfmt = outfmt or "otf"
        self.cfg = configparser.ConfigParser()
        self.cfg.read(configfile)

        #Parse config file for 'font' options: family name, variant name, version...
        if not self.cfg.has_section('font'):
            print(f"Section [font] missing in config file {configfile}" )
            sys.exit()
        font_config = {k:v for k,v in self.cfg.items('font')}
        fontfamily = font_config.get('family', None)
        if not fontfamily:
            print(f"'famly' missing in config file {configfile}" )
            sys.exit()
        fontname = font_config.get('name', None)
        if not fontname:
            print(f"'name' missing in config file {configfile}" )
            sys.exit()
        fontversion = font_config.get('version', None)
        if not fontversion:
            print(f"'version' missing in config file {configfile}" )
            sys.exit()
        fontcopyright = font_config.get('copyright', None)

        #Parse config file for 'source' options: source directory, feature file...
        if not self.cfg.has_section('source'):
            print(f"Section [source] missing in config file {configfile}" )
            sys.exit()
        source_config = {k:v for k,v in self.cfg.items('source')}
        self.sourcedir = source_config.get('glyphdir', None)
        if not self.sourcedir:
            print(f"'sourcedir' missing in config file {configfile}" )
            sys.exit()
        #TODO: if no svgs found in sourcedir, error out
        self.featurefile = source_config.get('featurefile', None)
        if self.featurefile and not os.path.exists(self.featurefile):
            print(f"{self.featurefile} does not exists")
            sys.exit()

        uc_glyph_map_file = source_config.get('ucglyphmapfile', None)
        if uc_glyph_map_file and not os.path.exists(uc_glyph_map_file):
            print(f"{uc_glyph_map_file} does not exists")
            sys.exit()
        source_config = {k:v for k,v in self.cfg.items('source')}
        uc_glyph_map_file = source_config.get('ucglyphmapfile', None)
        alt_glyph_suffix = '.alt'
        if uc_glyph_map_file:
            #Expected format:
            #Unicode-hex glyphname
            #0x0D05 ml_a
            with open(uc_glyph_map_file, 'r') as gu:
                self.glyhpname_ucode = {l.split(' ')[1]: l.split(' ')[0] for l in gu.read().lstrip('\n').rstrip('\n').split('\n')}

        #Scale svgs when importing?
        self.scaleglyphs = True if source_config.get('scaleglyphs', False) == 'True' else False
        print("scaleglyphs? "+str(self.scaleglyphs))

        #Set font metadata
        style = (fontname.split("-")[1] if '-' in fontname else 'Regular').title()
        if style.lower() == 'bolditalic':
            style = 'Bold Italic'
        fontname = fontname.split("-")[0]   #Strip the style/variant

        self.font = fontforge.font()

        if style != "Regular":      #'Font' instead of 'Font-Regular'
            self.font.fontname = (f"{fontname}-{style}").replace(" ", "")
            self.font.fullname = f"{fontname} {style}"
        else:
            self.font.fontname = fontname.replace(" ", "")
            self.font.fullname = fontname

        self.font.familyname = fontfamily
        self.font.weight     = style
        self.font.version    = fontversion
        self.font.encoding   = "Unicode"

        self.font.ascent     = int(font_config.get('ascent', 660))
        self.font.descent    = int(font_config.get('descent', 340))

        self.font.copyright  = fontcopyright or "Copyright ..."

    def get_glyph_ucode(self, glyphname):
        return int(self.glyhpname_ucode.get(glyphname, '0'), 16) or -1 if self.glyhpname_ucode else -1

    #Generate binary font: import glyph, add OTL ...
    def generate_font(self):

        self.import_glyphs()

        if self.featurefile:
            self.add_otl_feature()
        self.correct_glyphs()

        outfile = self.font.fontname + "." + outfmt

        if os.path.exists(outfile):
            os.remove(outfile)
        sfd = outfile.replace(outfmt, "sfd")
        if os.path.exists(sfd):
            os.remove(sfd)

        print(f"Generating {outfile}...")
        self.font.save()
        self.font.generate(outfile)

    def get_bearing_from_config(self, cfg_dict, key):
        #format: '10,26'
        defalt_left_bearing, default_right_bearing = [int(v) for v in cfg_dict.get("default", '30,30').split(',')]
        def_bearing_str = ','.join([str(defalt_left_bearing),str(default_right_bearing)])     #required if glyph has no overriding bearing in config
        if key == "default":
            return [defalt_left_bearing, default_right_bearing]
        else:
            return [int(v) for v in cfg_dict.get(key, def_bearing_str).split(',')]

    def import_glyphs(self):
        print(f"Importing svg glyphs from {self.sourcedir}...")

        glyph_files = glob.glob(os.path.join(self.sourcedir, "*.svg"))
        glyph_files.sort()

        glyphnames = set()
        bearing_config = {k:v for k,v in self.cfg.items('bearing')}      #(lef,right) bearing: default and override for individual glyphs
        #print(bearing_config)
        defalt_left_bearing, default_right_bearing = self.get_bearing_from_config(bearing_config, "default")
        for file in glyph_files:
            glyphname = os.path.splitext(os.path.basename(file))[0]
            glyphnames.add(glyphname)
            #Assign Unicode fromm config
            glyphucode = self.get_glyph_ucode(glyphname)

            #Handle stylistic alternates
            if '-' in glyphname:    #ch3ch3-1, ch3ch3-2 etc.
                baseg, sfx = glyphname.split('-')
                glyphname = baseg + ".ss%02d" % int(sfx)    #ch3ch3.ss01, ch3ch3.ss02 etc.
                script_tag = ot_script_from_ucode(self.get_glyph_ucode(baseg))  #OpenType script from codepoint of baseglyph

                if self.alternates.__contains__(baseg):
                    self.alternates[script_tag][baseg].append(glyphname)
                else:
                    self.alternates[script_tag][baseg] = [glyphname]


            #print(f"Creating {glyphname}")
            glyph = self.font.createChar(glyphucode, glyphname)
            glyph.importOutlines(file, scale=self.scaleglyphs, simplify=True, toobigwarn=True, correctdir=True, handle_eraser=True)
            glyph.removeOverlap()
            glyph_left_bearing, glyph_right_bearing = self.get_bearing_from_config(bearing_config, glyphname)    #or default bearing
            glyph.left_side_bearing = glyph_left_bearing
            glyph.right_side_bearing  = glyph_right_bearing

        #Add required glyphs like NUL/ZWJ/ZWNJ/.notdef, it not present in the font
        width_config = {k:v for k,v in self.cfg.items('glyphwidth')}
        for (glyphname, glyphucode) in [('NUL', 0x0000), ('space', 0x0020), ('ZWNJ', 0x200C), ('ZWJ', 0x200D), ('.notdef', -1)]:
            if glyphname not in glyphnames:
                print(f"Creating {glyphname}")
                glyph = self.font.createChar(glyphucode, glyphname)
                glyph.width = int(width_config.get("space", 240)) if glyphname == "space" else 1    #NOTE: 0 width glyphs will be discarded. TODO: width of space for bold/regular should be different

    def add_otl_feature(self):
        print("Adding Malayalam shaping rules...")
        #self.font.mergeFeature(self.featurefile, True)    #Ignore non-existent glyphs
        self.font.mergeFeature(self.featurefile)

        #If alternates are present, add lookup ss01, ss02 etc.
        ss_list = {s:{} for s in self.alternates.keys()} #{ 'latn': {'ss01': [(base,alt), ...], ...}, 'mlm2': {...,'ss20': [(base,alt), ...]} }
        for script_tag, glyph_alts in self.alternates.items():
            for baseglyph, alts in glyph_alts.items():
                for alternateglyph in alts:
                    ss_tag = alternateglyph[-4:]    #ch3ch3.ss01
                    if ss_tag in ss_list[script_tag]:
                        ss_list[script_tag][ss_tag].append((baseglyph, alternateglyph))
                    else:
                        ss_list[script_tag][ss_tag] = [(baseglyph, alternateglyph)] # ss01: [(ga,ga.ss01), (gb,gb.ss01), ...]
        if not ss_list:
            return
        #Write a feature file & merge it
        salt_fea_file = "stylistic-alternates.fea"
        with open(salt_fea_file, "w") as salt_f:
            for script_tag, ss_items in ss_list.items():
                for ss_tag, glyphset in ss_items.items():
                    fea_line = []
                    fea_line.append("\nfeature " + ss_tag + " {")  #Open feature 'ss01'
                    fea_line.append("\tscript " + script_tag + ";\n")  #mlm2 or latn, depending on glyph
                    for baseglyph, alternateglyph in glyphset:
                        #Add each glyph substitution
                        fea_line.append("\tsub " + baseglyph + " by " + alternateglyph + " ;")
                    fea_line.append("} " + ss_tag + " ;")        #Close feature
                    for l in fea_line:
                        salt_f.write(l+"\n")
            salt_f.close()
        self.font.mergeFeature(salt_fea_file)
        #os.remove(salt_fea_file)

    def correct_glyphs(self):
        print("Auto correcting glyphs...")

        if self.font.fullname.find("Slanted") or self.font.fullname.find("Italic"):
            (separation, minBearing, maxBearing)  = (40, 0, 20)
        else:
            (separation, minBearing, maxBearing)  = (140, 40, 80)

        self.font.round() # this one is needed to make simplify more reliable
        self.font.simplify()
        self.font.removeOverlap()
        #self.font.autoHint()

    def usage():
        print("Usage: %s <configfile> <otf|ttf|woff2>" % sys.argv[0])

if __name__ == "__main__":
    if len(sys.argv) != 3:
        usage()
        sys.exit()

    configfile = sys.argv[1]
    outfmt = sys.argv[2]
    if outfmt not in ("otf", "ttf", "woff2"):
        print("Only otf, ttf, woff2 font formats are supported")
        sys.exit()

    fmgr = FontBuilderSVG(configfile, outfmt)
    fmgr.generate_font()
