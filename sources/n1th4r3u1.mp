% vim: sw=2:sts=2:ts=2
% $Id: n1th4r3u1.mp,v 1.1 2023/04/04 03:24:52 cvr Exp $
%
% File: 'th1th4r3u1.mp'
% Time-stamp: <2023-04-03 08:12:36 cvr>
%
% Copyright (c) 2022, 2023, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
%
% $Header: /cvsroot/ml-font/n1th4r3u1.mp,v 1.1 2023/04/04 03:24:52 cvr Exp $
% $Date: 2023/04/04 03:24:52 $
% $Revision: 1.1 $
%

input mpost-defs;
input ml-shape-lib;
input option;
input out; 

%%% ന്ധ്രു %%%

input ml-glyphs-lib;

beginfig(00);
  % call the base glyph, which is preserved as a
  % picture variable gl_ndha. The argument (100) denotes
  % the point in the path from which to remove points.
  gl_ndha ;
  z.n1th4.4b'=(x.n1th4.4b,y.n1th4.4b);
  penpos.n1th4.4b'(wd.n1th4.4b,a_ea);
  
  % v/h-shift coors of of the reph sign
  dx20a=.1b; dy20a=-.5u; dy20b=0u; dx20d=-.7b;
  dx20g=-.4b; dx20f=.6b;
  wd20h=wd5b; wd20g=w_sw;
  % coors of reph based on the bbox of base glyph
  coor_c_reph (20)(0,0,.35b,6u);
  % width/angles of coors
  penpos_c_reph (20) ;

  % define vertical correction to suit each variant.
%  make_cor (vcor_u) (.6wd21b)(2wd21b)(1.1wd21b)(2.5wd21b);
  make_cor (vcor_u) (1wd21b)(3wd21b)(2wd21b)(8wd21b);
  
  % stroke the path directly to shape it
  % around the base glyph instead of resorting
  % to strenuous generalization by way of a function.
  make_reph_vl_u (z.n1th4.4b'.e{dir 270}..
    z20g.e ..z20f.e ... z20d.e ..z20c.e ..z20b.e ..z20a.e) ;
  % show the labels
  penlabels(20a,20b,20c,20d,20f,20g,20h);
endfig;  

end;

$Log: n1th4r3u1.mp,v $
Revision 1.1  2023/04/04 03:24:52  cvr
initial commit.






  
