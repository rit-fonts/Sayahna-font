% vim: sw=2:sts=2:ts=2
% $Id: t3r3u2.mp,v 1.2 2023/04/02 07:44:18 cvr Exp $
%
% File: 't3r3u2.mp'
% Time-stamp: <2023-04-01 09:06:31 cvr>
%
% Copyright (c) 2022, 2023, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
%
% $Header: /cvsroot/ml-font/t3r3u2.mp,v 1.2 2023/04/02 07:44:18 cvr Exp $
% $Date: 2023/04/02 07:44:18 $
% $Revision: 1.2 $
%

input mpost-defs;
input ml-shape-lib;
input option;
input out; 

%%% ഡ്രൂ %%%

input ml-glyphs-lib;

beginfig(00);
  % call the base glyph, which is preserved as a
  % picture variable gl_gddha. The argument (100) denotes
  % the point in the path from which to remove points.
  gl_Da(Da) ;

  z.Da.4b'=(x.Da.4b,y.Da.4b); penpos.Da.4b'(wd.Da.4b,a_ea);
  
  % v/h-shift coors of of the reph sign
  dx20a=.1b; dy20a=-.5u; dx20b=-.2b; dy20b=4u; dx20d=-.7b;
  dx20g=-.45b; dx20f=.5b;  wd20h=wd4b; wd20g=w_se+.5t;
  % coors of reph based on the bbox of base glyph
  coor_c_reph (20)(0,0,.1b,5u);
  % width/angles of coors
  penpos_c_reph (20) ;

  % define vertical correction to suit each variant.
  % make_cor (vcor_uu) (.7wd21b)(1wd21b)(.7wd21b)(2wd21b);  
  make_cor (vcor_uu) (.7wd21b)(2wd21b)(1wd21b)(3wd21b);  

  % stroke the path directly to shape it
  % around the base glyph instead of resorting
  % to strenuous generalization by way of a function.
  make_reph_vl_uu ( z.Da.4b'.e{dir 270}..
    z20g.e ..z20f.e ..z20d.e
    ..z20c.e ..z20b.e ..z20a.e ) ;
  % show the labels
  penlabels(20a,20b,20c,20d,20f,20g,20h,20i);
endfig;  

end;

$Log: t3r3u2.mp,v $
Revision 1.2  2023/04/02 07:44:18  cvr
revised as per new repham function.

Revision 1.1  2023/03/15 09:12:20  cvr
t3r3u1: initial commit.






  
