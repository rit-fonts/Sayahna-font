% vim: sw=2:sts=2:ts=2
% $Id: th1th1r3u2.mp,v 1.2 2023/04/02 07:46:35 cvr Exp $
%
% File: 'th1th1r3u2.mp'
% Time-stamp: <2023-04-02 11:02:12 cvr>
%
% Copyright (c) 2022, 2023, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
%
% $Header: /cvsroot/ml-font/th1th1r3u2.mp,v 1.2 2023/04/02 07:46:35 cvr Exp $
% $Date: 2023/04/02 07:46:35 $
% $Revision: 1.2 $
%

input mpost-defs;
input ml-shape-lib;
input option;
input out; 
  
%%% ത്ത്രൂ %%%

input ml-glyphs-lib;

beginfig(00);
  % call the base glyph, which is preserved as a
  % picure variable gl_ththa. The value 6 xstp denotes
  % the point in the path from which to remove points.
  dy.th1th1.2kd=-2u; xstp:=6;
  gl_ththa ;

  % chop the x-dimen from the bbox of base glyph.
  % it refers to the x-dimen of points removed.
  xchop(x.th1th1.2kd-x.th1th1.2h);
  % v/h-shift coors of of the reph sign
  dx20a=.2b; dy20a=-.5u; dy20b=4u;
  dx20d=-.4b; dx20f=1b; dx20g=.5b;
  % coors of reph based on the bbox of base glyph
  coor_c_reph (20)(0,0,.3b,5u);
  % width/angles of coors
  penpos_c_reph (20) ;

  % define vertical correction to suit each variant.
  make_cor (vcor_uu) (.7wd21b)(2wd21b)(1.2wd21b)(3.4wd21b);  

  % stroke the path directly to shape it
  % around the base glyph instead of resorting
  % to strenuous generalization by way of function.
  make_reph_vl_uu (z.th1th1.2h.e{dir -5} ..z.th1th1.2kd.e {dir 270} 
    ..z20g.e ..z20f.e ..z20d.e ..z20c.e ..z20b.e ..z20a.e) ;
  % show the labels
  penlabels(20a,20b,20c,20d,20f,20g,20h,20i);
endfig;  

end;

$Log: th1th1r3u2.mp,v $
Revision 1.2  2023/04/02 07:46:35  cvr
revised as per new repham function.

Revision 1.1  2023/03/16 05:32:20  cvr
th1th1r3u2: initial commit.

