% vim: sw=2:sts=2:ts=2
% $Id: njch2u1.mp,v 1.3 2023/03/11 05:46:00 cvr Exp $
%
% File: 'njch2u1.mp'
% Time-stamp: <2023-03-11 11:15:00 cvr>
% 
% Copyright (c) 2022, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
%
% $Header: /cvsroot/ml-font/njch2u1.mp,v 1.3 2023/03/11 05:46:00 cvr Exp $
% $Date: 2023/03/11 05:46:00 $
% $Revision: 1.3 $
%
% ശ്ച 
%
input mpost-defs;
input ml-shape-lib;
input option;
input out; 

getver(1.2);

beginfig(33);
  % coors of llobe
  coor_c_llobe_eye (1) (0,0);
  % stroke llobe
  pstroke_c_llobe_eye (1);

  % coors of first mlobe
  coor_c_mlobe (1') (x1kf.r-.5wd1'b,0);
  stop:=2;
  % stroke mlobe
  pstroke_c_mlobe (1') ;
  
  % adjust width of 2c; vshift 2f of right part of ര 
  % h/vshift the coor 2h
  dy2h=3.5u; dx2h=-.1b;  dy2f=5u;
  % coors of ര 
  coor_g_rha (2) (x1'c-(x2g-x2b),0);
  % remove path before (2) and keep till end
  start:=2; stop:=infinity;
  % equalize x-val of 2c with 1'c;
  % equalize width of 2h with 2h
  x2c:=x1'c; wd2h=wd2c;
  % stroke the path of right of ര 
  pstroke_g_rha (2);

  % stroke the hstem 
  pstroke_stem (3) (x2g.r,1.5u,3b,.8w_w);

  % define coors of the curve connecting first ര with hstem 
  z5a=(.5[x2d,x3c],y2d); penpos5a(wd2d,ang2d);
  z5b=(.45[x3b,x3c],.5[y3a,y3b]); penpos5b(w_s,a_ea);
  % stroke the path
  penstroke z2h.e+(0,0){dir 15} .. z5a.e{dir 270} .. z5b.e ;
  % show the labels
  penlabels(5a,5b);

  % vshift 6a, 6f; adjust angle of 6a, all of second ര 
  dy6a=1.5u+.35wd1a; ang6a=210; dy6f=2u;
  % coors of second ര 
  coor_g_rha(6) (x3d-(x6a-x6b)+5,0);
  % do not remove any points from path
  start:=0;
  % vshift 6g and 6d
  y6g:=.5[y6a,y6c]; y6d:=y6g;
  % stroke the path of second ര 
  pstroke_g_rha (6);
  % stroke the edge that connects first coor of second ര with hstem
  pstroke_edge ((x6a.r-.1b,1.5u),(x6a.r-.1b,y6a.l),z6a.l,(x6a.l,1.5u));

  % coors of bottom vowel sign (hstems)
  coor_u_bot (7) (x6h,-1u);
  % adjust width of and hshift a few coors
  wd7h=wd6h; y7h:=y6h; x7pc:=x3b+.25wd7pc;  x7pb:=x7pc+.5b; x7pd:=x7pb;
  % stroke the bottom vowel sign hstems
  pstroke_u_bot (7);  
endfig;

end;

$Log: njch2u1.mp,v $
Revision 1.3  2023/03/11 05:46:00  cvr
njch2u1: fixed a few errors in the comments.

Revision 1.2  2023/03/11 05:38:55  cvr
njch2u1: revised per w/a.



  
