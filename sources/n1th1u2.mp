% vim: sw=2:sts=2:ts=2
% $Id: n1th1u2.mp,v 1.3 2023/03/10 02:32:16 cvr Exp $
%
% File: 'n1th1u2.mp'
% Time-stamp: <2023-03-10 08:01:19 cvr>
% 
% Copyright (c) 2022, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
%
% $Header: /cvsroot/ml-font/n1th1u2.mp,v 1.3 2023/03/10 02:32:16 cvr Exp $
% $Date: 2023/03/10 02:32:16 $
% $Revision: 1.3 $
%
input mpost-defs;
input ml-shape-lib;
input option;
input out;

getver(1.2);

% ന്തൂ 

beginfig(34);
  % v/hshift a few coors
  dy1a=dy1f=3.5u; dy1b=1.75u; dx1c=-.075b;
  dy1d=.5u; dx1d=-.15b;
  % coor of left lobe
  coor_c_llobe (1) (0,0);
  % stroke the path left lobe
  pstroke_c_llobe (1); 

  % h/vshift some of the coors
  dx2h=.25b; dy2a=-0.75u; dy2c=dy2h=-3.5u;
  dy2b=dy2g=dy2d=-4u; dx2c=-.075b; dx2d=-.05b; dx2f=-.05b;
  % coors of ത part
  coor_c_tha_second (2) (x1d.r-.5wd2b,3.5u);
  % remove path after point 6
  stop:=5;
  % stroke the path
  pstroke_c_tha_second (2);

  % hshift a few coors
  dy3f=.5u; dx3h=-.3b; dx3i=-.3b; dx3d=-.1b;
  % coors of right part of ദ 
  coor_c_rlobe_da (3) (x2h-(x3c-x3b)+.2b,0);
  % adjust various width/angle of coors
  wd3c=wd2h; ang3h=180; ang3i=ang3j=90;
  % adjust h/v-shift of a few coors
  x3d:=x3h; x3g:=x3g-.2b; x3j:=x3g; y3j:=y3i-2;
  % remove the entire path
  stop:=0; stop_i:=0;
  % stroke the path of right part of ദ 
  pstroke_c_rlobe_da (3);
  % custom stroke to connect 2g through 3c to 3g
  penstroke z2g.e{up} ..z3c.e+(-.6b,0) {right} ..z3d.e  {dir 270}
    .. {dir 185}z3g.e;
  penlabels(2g,3c,3d,3g);
  
  % coors of horizontal vowel sign
  coor_u_bot (4) (x3h,0);
  % remove path before point 1 and keep path till end
  start:=2; stop:=infinity;

  wd4pa=wd4pb=wd3i; x4pf:=x3h+.5wd4pf; 
  % hshift the horizontal line to match the width of glyph
  x4pa:=x3i; y4pa:=y3i; x4pc:=x1b+.25wd4pc; x4pb:=x4pc+.4b; x4pd:=x4pb;
  % stroke the path of vowel sign
  pstroke_u_bot (4);

  % custom stroke for path connecting 2g thru 3h to 2kd
  penstroke z3g.e {dir 5} .. z3h.e ..{dir 180} z3i.e;
  penlabels(3g,3h,3i,5a);

endfig;

end;

$Log: n1th1u2.mp,v $
Revision 1.3  2023/03/10 02:32:16  cvr
n1th1u2: small refinements (3g).

Revision 1.2  2023/03/10 02:27:04  cvr
n1th1u2: revised per a/w.
















