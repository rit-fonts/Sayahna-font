% vim: sw=2:sts=2:ts=2
% $Id: njch3u1.mp,v 1.2 2023/03/11 07:33:37 cvr Exp $
%
% File: 'njch3u1.mp'
% Time-stamp: <2023-03-11 13:00:57 cvr>
% 
% Copyright (c) 2022, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
%
% $Header: /cvsroot/ml-font/njch3u1.mp,v 1.2 2023/03/11 07:33:37 cvr Exp $
% $Date: 2023/03/11 07:33:37 $
% $Revision: 1.2 $
%
% ഞ്ജ 
%
input mpost-defs;
input ml-shape-lib;
input option;
input out; 

beginfig(33);
  % coors of llobe
  coor_c_llobe_eye (1) (0,0);
  % stroke llobe
  pstroke_c_llobe_eye (1);

  % coors of mlobe
  coor_c_mlobe (1') (x1kf.r-.5wd1'b,0);
  stop:=2;
  % stroke mlobe
  pstroke_c_mlobe (1') ;

  % h/vshift coors of ജ to accommodate the
  % vowel sign at the bottom
  dy2a=dy2b'=dy2c=dy2c'=dy2d=dy2d'=dy2h=1u;
  dy2k=dy2l=2u; dy2m=.75u; dy2o=1.5u;
  dx2c=-.5u; dx2g=-1u; dx2i=dx2p=0u; dx2n=-2u;

  % remove the path till (2)
  start:=2; stop:=infinity;
  % coors of ജ 
  coor_g_ja (2)(x1'c,0u);
  % stroke the path of ജ 
  pstroke_g_ja (2);

  % stroke the connection between the end of mlobe
  % and the left eye of ജ 
  penstroke z1'c.e{dir 0} .. {dir 270} z2c'.e;

  % h/vshift coors of vowel sign to streamline
  dx3a=7u; dx3aa=-1u; start:=0;
  % coors of bottom vowel sign
  coor_u_i_bot (3) (.1b,-1u,1.5b,x2n+5u,y2n);
  % hshift the coors of bottom hstems
  x3a:=x2o; x3c:=x2j; x3b:=x3c+.3b; x3d:=x3b;
  x3f:=x3aa+.5wd3f;  ang3aa:=0; wd3aa:=w_ea; stop_i:=0;
  % stroke the path
  pstroke_u_i_bot (3) ;

  % define coors to connect top of last eye of ജ
  % with top right of hstem of bottom vowel sign
  z3aa'=(x2m,y2m); penpos3aa'(wd2m,90);
  % stroke the custom path
  penstroke z3aa'.e{dir 2} .. z3aa.e ..{dir 180} z3a.e;
  % show the labels
  penlabels(3aa,3a,3aa');
endfig;

end;

$Log: njch3u1.mp,v $
Revision 1.2  2023/03/11 07:33:37  cvr
njch3u1: revised per w/a.






  
