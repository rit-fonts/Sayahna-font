% vim: sw=2:sts=2:ts=2
% $Id: shr3.mp,v 1.1 2023/04/05 05:49:10 cvr Exp $
%
% File: 'shr3.mp'
% Time-stamp: <2023-04-05 10:35:10 cvr>
%
% Copyright (c) 2022, 2023, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
%
% $Header: /cvsroot/ml-font/shr3.mp,v 1.1 2023/04/05 05:49:10 cvr Exp $
% $Date: 2023/04/05 05:49:10 $
% $Revision: 1.1 $
%

input mpost-defs;
input ml-shape-lib;
input option;
input out; 
input ml-vlsigns-lib;

%%% ഷ്ര %%%

input ml-glyphs-lib;

beginfig(00);
  % bbox correction to compensate protrusion of label in draft mode.
  xrchop:=2.5mm;
  
  % call the base glyph, which is preserved as a
  % picture variable gl_nma.
  gl_sha;

  % v/h-shift coors of of the reph sign
  dx20a=0b; dy20a=-.5u; dy20b=5u; dy20c=-2u;
  dx20d=.2b; dx20f=-.4b; dy20d=dy20f=-.5u;
  wd20h=thick; wd20g=thick+.25t; ang20g=a_ea;
  % coors of reph based on the bbox of base glyph
  coor_c_reph (20)(0,0,.2b,6u);

  x20g:=x20h:=.5[x.sh.4a,x.sh.4d]; y20h:=y.sh.4d; y20g:=y20h-1u;
  wd20h:=wd20g:=x.sh.4d-x.sh.4a;
  
  % width/angles of coors of reph;
  penpos_c_reph (20) ;

  % stroke the path directly to shape it
  % around the base glyph instead of resorting
  % to strenuous generalization by way of a function.
  make_reph (z20h.e{dir 270} ..z20g.e ..z20f.e 
     ... z20d.e ..z20c.e{dir 135} ..z20a.e) ;
  % show the labels
  penlabels(20a,20b,20c,20d,20f,20g,20h);
endfig;  

end;

$Log: shr3.mp,v $
Revision 1.1  2023/04/05 05:49:10  cvr
initial commit.





