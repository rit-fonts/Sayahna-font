% vim: sw=2:sts=2:ts=2
% $Id: n1th3r3u1.mp,v 1.2 2023/04/02 07:49:08 cvr Exp $
%
% File: 'n1th3r3u1.mp'
% Time-stamp: <2023-04-02 12:46:43 cvr>
%
% Copyright (c) 2022, 2023, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
%
% $Header: /cvsroot/ml-font/n1th3r3u1.mp,v 1.2 2023/04/02 07:49:08 cvr Exp $
% $Date: 2023/04/02 07:49:08 $
% $Revision: 1.2 $
%

input mpost-defs;
input ml-shape-lib;
input option;
input out; 
input ml-vlsigns-lib;

%%% ന്ദ്രു %%%

input ml-glyphs-lib;

beginfig(00);
  % call the base glyph, which is preserved as a
  % picture variable gl_nda. The argument (6) denotes
  % the point in the path from which to remove points.
  dy.n1th3.2f=-1.5u; dy.n1th3.2h=-1.5u; ang.n1th3.2h=a_w; xstp:=2;
  gl_nda;
  % define a coor with same x,y as x1h to reverse the angle. 
  z.n1th3.2h'=(x.n1th3.2h,y.n1th3.2h);
  penpos.n1th3.2h'(wd.n1th3.2h,a_ea);

  % v/h-shift coors of of the reph sign
  dx20a=0b; dy20a=-.5u; dy20b=4u; dx20c=0b;
  dx20f=-.2b; dx20g=-.25b; ang20g=a_se+15;  
  % coors of reph based on the bbox of base glyph
  coor_c_reph (20)(0,0,.3b,6u);
  % width/angles of coors
  penpos_c_reph (20) ;

  % define vertical correction to suit each variant.
  make_cor (vcor_u) (1wd21b)(2wd21b)(1wd21b)(4.5wd21b);  
  
  % stroke the path directly to shape it
  % around the base glyph instead of resorting
  % to strenuous generalization by way of a function.
  make_reph_vl_u (z.n1th3.2h'.e{dir 270} .. %z20g.e ..
    z20f.e ..z20d.e ..z20c.e ..z20b.e ..z20a.e) ;
  % show the labels
  penlabels(20a,20b,20c,20d,20f,20g,20h);
endfig;  

end;

$Log: n1th3r3u1.mp,v $
Revision 1.2  2023/04/02 07:49:08  cvr
revised as per new repham function.

Revision 1.1  2023/03/18 11:22:01  cvr
n1th3r3u1: initial commit.



