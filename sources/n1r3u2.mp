% vim: sw=2:sts=2:ts=2
% $Id: n1r3u2.mp,v 1.1 2023/04/04 03:26:25 cvr Exp $
%
% File: 'n1r3u2.mp'
% Time-stamp: <2023-04-03 11:42:38 cvr>
%
% Copyright (c) 2022, 2023, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
%
% $Header: /cvsroot/ml-font/n1r3u2.mp,v 1.1 2023/04/04 03:26:25 cvr Exp $
% $Date: 2023/04/04 03:26:25 $
% $Revision: 1.1 $
%

input mpost-defs;
input ml-shape-lib;
input option;
input out; 

%%% ന്രൂ %%%

input ml-glyphs-lib;

beginfig(00);
  % call the base glyph, which is preserved as a
  % picure variable gl_na. The argument (6) denotes
  % the point in the path from which to remove points.
  xstp:=2; dy.n1.2d=-2u;
  gl_na ;

  % chop the x-dimen from the bbox of base glyph.
  % it refers to the x-dimen of points removed.
  make_cor (c_cor)(33)(16)(15)(15);
  xchop(x.n1.2d-x.n1.2c+c_cor);
  
  % v/h-shift coors of of the reph sign
  dx20a=.2b; dy20a=-.5u; dy20b=4u;
  dx20d=-0b; dx20f=.5b;
  dx20g=.5b;
  wd20f=w_se; ang20g=a_se+15;
  % coors of reph based on the bbox of base glyph
  coor_c_reph (20)(0,0,.3b,5u);
  % width/angles of coors
  penpos_c_reph (20) ;

  % define vertical correction to suit each variant.
  make_cor (vcor_uu) (.5wd21b)(1.5wd21b)(.6wd21b)(2wd21b);  

  % stroke the path directly to shape it
  % around the base glyph instead of resorting
  % to strenuous generalization by way of function.
  wd.n1.2d:=w_ea; 
  make_reph_vl_uu (z.n1.2c.e{dir -5} ..
      z.n1.2d.e {dir 270} ..
    z20g.e ..z20f.e ..z20d.e ..z20c.e ..z20b.e ..z20a.e) ;
  % show the labels
  penlabels(n1.1h,n1.1kd,20a,20b,20c,20d,20f,20g,n1.2c,n1.2d);
endfig;  

end;

$Log: n1r3u2.mp,v $
Revision 1.1  2023/04/04 03:26:25  cvr
initial commit.






