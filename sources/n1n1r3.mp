% vim: sw=2:sts=2:ts=2
% $Id: n1n1r3.mp,v 1.2 2023/04/04 04:48:40 cvr Exp $
%
% File: 'n1n1r3.mp'
% Time-stamp: <2023-04-04 09:47:26 cvr>
%
% Copyright (c) 2022, 2023, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
%
% $Header: /cvsroot/ml-font/n1n1r3.mp,v 1.2 2023/04/04 04:48:40 cvr Exp $
% $Date: 2023/04/04 04:48:40 $
% $Revision: 1.2 $
%

input mpost-defs;
input ml-shape-lib;
input option;
input out; 

%%% ന്ന്ര %%%

input ml-glyphs-lib;

beginfig(00);
  % vshift the coor n1th1.2kd of ന്ത
  dy.n1n1.2kd=-2u;
  % call the base glyph, which is preserved as a
  % picure variable gl_nna. The xstp argument (6) denotes
  % the point in the path from which to remove points.
  xstp:=2; 
  gl_nna ;

  % chop the x-dimen from the bbox of base glyph.
  % it refers to the x-dimen of points removed.
  make_cor (c_cor)(36)(18)(16)(8);
  xchop(x.n1n1.3d-x.n1n1.3c+c_cor);

  % v/h-shift coors of of the reph sign
  dx20a=.1b; dy20a=-.5u; dy20b=4u; dy20d=dy20f=-.3u;
  dx20d=-.2b; dx20f=.7b; dx20g=.45b;
  wd20g=w_sw;
  ang20f=a_s; ang20d=a_s-10;

  % coors of reph based on the bbox of base glyph
  coor_c_reph (20)(0,0,.3b,4u);
  % width/angles of coors
  penpos_c_reph (20) ;

  % stroke the path directly to shape it
  % around the base glyph instead of resorting
  % to strenuous generalization by way of function.
  make_reph (z.n1n1.3c.e{dir -5} .. z.n1n1.3d.e {dir 270} ..
      z20g.e .. z20f.e .. z20d.e ..z20c.e ..z20b.e ..z20a.e) ;
  % show the labels
  penlabels(20a,20b,20c,20d,20f,20g,n1n1.3c,n1n1.3d);
endfig;  

end;

$Log: n1n1r3.mp,v $
Revision 1.2  2023/04/04 04:48:40  cvr
bbox calculation error fixed.

Revision 1.1  2023/04/04 03:25:18  cvr
initial commit.





