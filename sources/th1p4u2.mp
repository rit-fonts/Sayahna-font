% vim: sw=2:sts=2:ts=2
% $Id: th1p4u2.mp,v 1.2 2023/03/07 13:10:52 cvr Exp $
%
% File: 'th1p4u2.mp'
% Time-stamp: <2023-03-07 16:49:48 cvr>
% Copyright (c) 2022, Rachana Institute of Typography (RIT), 
% Trivandrum 695014, India
% email: <info@rachana.org.in>
% url: www.rachana.org.in
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   http://www.latex-project.org/lppl.txt
% and version 1.3 or later is part of all distributions of LaTeX
% version 2005/12/01 or later.
%
% This work has the LPPL maintenance status “maintained”.
% 
% The Current Maintainer of this work is RIT Team.
% $Header: /cvsroot/ml-font/th1p4u2.mp,v 1.2 2023/03/07 13:10:52 cvr Exp $
% $Date: 2023/03/07 13:10:52 $
% $Revision: 1.2 $
%
% ത്ഭൂ - th1p4u2
%
input mpost-defs;
input ml-shape-lib;
input option;
input out; 

getver(1.2);

beginfig(34);
  % vshift the coors of ത part
  dy11c=dy11h=dy11kd=dy11kf=-2u;
  dy11b=dy11g=dy11d=-1u;
  % coors of ത part
  coor_c_tha (11) (0,2u);
  % stroke the path
  pstroke_c_tha (11);

  % extend the vstem of ത  to baseline
  pstroke_stem (12) (x11kf.l,2u,wd11kf,.5h);

  % hshift coor 2c by 2 units
  dx1c=.2b;
  % coors of right lobe of ന 
  coor_c_rlobe (1) (x11kf.r-.5wd1a,0);
  % remove the path after point number 2
  stop:=2;
  % stroke the path of right lobe of  ന
  pstroke_c_rlobe (1); 

  % vshift coors of right lobe of ഭ 
  dy2a=-2u; dy2f=-1u; dy2g=-.5u;
  % coors of right lobe of  ഭ 
  coor_c_bha_rt (2) (x1c-.2b,2u);
  % start the path at point 0 and remove after 4
  start:=0; stop:=4;
  % stroke the right part of ഭ 
  pstroke_c_bha_rt (2);

  % hshift the coors to adjust
  dx3kc=-.15b; dx3kd=-.3b;
  % coors of vowel signs of കു 
  coor_c_ku_rt (3) (x2h-(x2c-x2f-.4wd2c),-1.4u);
  % remove path until 1; adjust angle
  start:=1; if (wa=4): ang3ka=150; fi
  % stroke the vowel sign part of കു 
  pstroke_c_ku_rt (3);
  % stroke the path from last but first coor of
  % ഭ to point 1 of vowel sign of കു
  % to streamline the path
  penstroke z2h.e {dir 14} .. {down}z3kb.e;

  % coors of bottom lines
  coor_u_bot (4)(x3kg+.2b,0);
  % start path at point 2 and traverse untill end
  start:=2; stop:=infinity;
  % adjust the widths of strokes
  wd4pa=wd4pb=wd3kg;
  % h/v-shift coors to refine
  x4pc:=x11b+.5wd4pc; x4pb:=x4pd:=x4pc+.4b;
  x4pf:=x3kf+.4wd3kf; y4pa:=y4pb:=y3kg;
  y4pc:=.5[y4pb,y4pd];
  % stroke the bottm lines of the vowel sign
  pstroke_u_bot (4);
endfig;

end;


$Log: th1p4u2.mp,v $
Revision 1.2  2023/03/07 13:10:52  cvr
th1p4u2: revised per w/a scheme.

