#  vim: sw=2:sts=2:ts=2
#  $Id: Makefile,v 1.2 2022/08/29 12:01:03 cvr Exp $
#  $Date: 2022/08/29 12:01:03 $
# 
#  File: 'ml-mpost-options.mp'
#  Time-stamp: <2023-02-14 17:02:28 cvr>
#
#  Copyright (c) 2022, Rachana Institute of Typography (RIT), 
#  Trivandrum 695014, India
#  email: <info@rachana.org.in>
#  url: www.rachana.org.in
# 
#  This work may be distributed and/or modified under the
#  conditions of the LaTeX Project Public License, either version 1.3
#  of this license or (at your option) any later version.
#  The latest version of this license is in
#    http://www.latex-project.org/lppl.txt
#  and version 1.3 or later is part of all distributions of LaTeX
#  version 2005/12/01 or later.
# 
#  This work has the LPPL maintenance status “maintained”.
#  
#  The Current Maintainer of this work is RIT Team.
# 

SHELL:=/bin/bash

###### METAFONT targets ######'

f=t4

n=3
v=0
-include version.mk

sans:
	echo 'width_angle(2);' > option.mp;
	echo 'wa_n:=2;' >> option.mp;

serif:
	echo 'width_angle(1);' > option.mp;
	echo 'wa_n:=1;' >> option.mp;

sansthin:
	echo 'width_angle(4);' > option.mp;
	echo 'wa_n:=4;' >> option.mp;

serifthin:
	echo 'width_angle(3);' > option.mp;
	echo 'wa_n:=3;' >> option.mp;

default:
	echo 'width_angle(0);' > option.mp;
	echo 'wa_n:=1;' >> option.mp;

proof:
	echo 'proofing:=3;' >> option.mp;

final:
	echo 'proofing:=0;' >> option.mp;

mps:
	echo 'outputtemplate := "%j.mps";'> out.mp 
	mpost $(f).mp ;
	mptopdf $(f).mps ;
	mv $(f)-mps.pdf $(f).pdf ;
	cp $(f).pdf chr.pdf;

mp:
	mpost $(f) ;

xsvg:
	echo 'outputformat := "svg";'> out.mp;
	echo 'outputtemplate := "%j.svg";'>> out.mp 
	make mp ;
	cp $(f).svg chr.svg;

pdf: mps
	cat version.mk;

png:
	echo 'outputformat := "png";'> out.mp;
	echo 'outputtemplate := "%j.png";'>> out.mp 
	make mp ;

view:
	xdg-open $(f).pdf ;

pngview:
	xdg-open $(f).png ;

svgview:
	xdg-open $(f).svg;

gallery:
	echo "\galleryinclude{$(f)}{$(n)}" > gal.tex ;
	pdflatex gallery.tex ;

gal: gallery

gview:
	xdg-open gallery.pdf ;


# verioned pdf:

vpdf:
	echo 'outputtemplate := "%j.mps";'> out.mp 
	gmake mp ;
	mptopdf $(f).mps ;
	cp $(f)-mps.pdf chr.pdf;
	cp $(f)-mps.pdf $(f).pdf;
	mv $(f)-mps.pdf $(f)-$(v).pdf ;

cppdf:
	cp $(f)-$(v).pdf pdf/;

spdf:
	cp $(f).pdf $(f)-sf.pdf;
	cp $(f)-sf.pdf pdf/;

cpsvg:
	cp $(f).svg svg/;

cpgallery:
	cp gallery.pdf pdf/$(f)-gallery.pdf ;
	cp gallery.pdf $(GIT)/$(f)-gallery.pdf ;
#	cp gallery.pdf new/$(f)-gallery.pdf ;

cpgal:
	@gmake cpgallery;

mview:
	xdg-open ../mpost/$(f).pdf 2>/dev/null

msrc:
	gvim ../mpost/$(f).mp 2>/dev/null

char: serif final pdf
	#Generate pdf & svg into separate directories
	cp $(f).pdf serif-pdf/;
	$(MAKE) --no-print-directory serif final xsvg;
	mv $(f).svg serif-svg/;

olap:
	#Remove overlap
	inkscape --batch-process --actions="select-all;path-union;export-overwrite;export-do" serif-svg/$(f).svg ; \
	inkview serif-svg/$(f).svg 2>/dev/null 
